package pl.net.nosal;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        TcpClient client = new TcpClient();
        client.startConnection("127.0.0.1",10000);
        Scanner scanner = new Scanner(System.in);

        while (client.getState()){
            String input = scanner.nextLine();
            if( input.equals("stop")){
                client.sendMessage(input);
                client.stopConnection();
            }else{

            System.out.println(client.sendMessage(input));}
        }
    }
}

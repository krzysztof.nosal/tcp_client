package pl.net.nosal;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class TcpClient {
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private boolean isRunning;
    public void startConnection(String ip, int port) {
        try{
        clientSocket = new Socket(ip, port);
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        isRunning=true;
        }
        catch (Exception e){
            System.out.println("error");
        }
    }

    public String sendMessage(String msg) {
        try{
        out.println(msg);
        String resp = in.readLine();
            return resp;
        }catch (Exception e){
            System.out.println("error");
            return "error";
        }

    }

    public void stopConnection() {
        try{
            isRunning=false;
        in.close();
        out.close();
        clientSocket.close();}
        catch (Exception e){
            System.out.println("error");
    }

}
public boolean getState(){
        return isRunning;
}

}

